import styles from './css/mycssmodule.module.css'
import { Routes, Route } from "react-router-dom";
import Home from './components/Home';
import GetCat from './components/GetCat';
import CreatePickup from './components/CreatePickup';
import GetProduct from './components/GetProduct';

function App() {
  return (
    <div className={styles.app}>
      <Routes>
        <Route path="product/:id" element={<GetProduct />} />
        <Route path="/" element={<Home />} />
        <Route path="type/:cat" element={<GetCat />} />
      </Routes>
    </div>
  );
}

export default App;