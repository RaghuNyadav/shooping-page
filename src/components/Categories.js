import React from "react";
import styles from '../css/mycssmodule.module.css'
import { Link } from 'react-router-dom'

import monkey from '../img/monkey.gif'


export default class Categories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }
    makeCategoryComp = (name) => {
        return (
            <div>
                <Link className={styles.link} to={`type/${name}`}>
                    <div className={styles.categorie}>{name}</div>
                </Link>
            </div>
        )
    }
    componentDidMount() {
        fetch("https://fakestoreapi.com/products/categories")
            .then(res => res.json())
            .then(
                (result) => {
                    // console.log(result)

                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                    // console.log(this.state.items)

                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className={styles.monkey}><img src={monkey} alt="loading.." /></div>;
        } else {
            return (
                <>
                    <div className={styles.catHead}>Categories</div>
                    <div className={styles.categories}>
                        {this.state.items.map(cat => <div>{this.makeCategoryComp(cat)}</div>)}
                    </div>
                </>
            );
        }
    }
}