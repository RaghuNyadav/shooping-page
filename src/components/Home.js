import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import Categories from "./Categories";
import CatType from "./CatType";
import CreatePickup from "./CreatePickup";
import shopImg from '../img/shop3.png'
import styles from '../css/mycssmodule.module.css'


export default class Home extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <img src={shopImg} className={styles.shoping} alt="shopping" />
                <CreatePickup />
                <Categories />
                {/* <CatType /> */}
                <Footer />
            </div>
        )
    }
}