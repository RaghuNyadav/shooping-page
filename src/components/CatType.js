import React from "react";
import styles from '../css/mycssmodule.module.css'
import heart from '../img/heart.gif'
import UpdateProduct from "./UpdateProduct";
import DeleteProduct from "./DeleteProduct";

export default class CatType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            isUpdated: null,
            isDeleted: false,
            start: true
        };
    }

    deleteProduct = ({ id, image, title, description, price, category }) => {
        let obj = {}
        obj['title'] = title
        obj['price'] = price
        obj['description'] = description
        obj['category'] = category
        obj['image'] = image
        obj['id'] = id
        obj['availabe'] = "Out of stock"
        setTimeout(() => {
            let arr = []
            this.state.items.map(item => {
                if (item.id === Number(id)) {
                    arr.push(obj)
                } else {

                    arr.push(item)
                }
            })

            this.setState({ items: arr, isDeleted: true, isUpdated: false })
            // console.log(obj, this.state.isDeleted)
        }, 2000)
    }

    updateProduct = ({ id, image, title, description, price, category }) => {
        let obj = {}
        obj['title'] = title
        obj['price'] = price
        obj['description'] = description
        obj['category'] = category
        obj['image'] = image
        obj['id'] = id
        console.log(obj)
        setTimeout(() => {
            let arr = []
            this.state.items.map(item => {
                if (item.id === Number(id)) {
                    arr.push(obj)
                } else {

                    arr.push(item)
                }
            })
            // console.log(arr)
            this.setState({ items: arr, isUpdated: true, start: false })
        }, 2000)
    }

    makeCategoryComp = ({ image, title, price, description, availabe }) => {

        return (
            <div className={styles.productBox}>
                <div><img className={styles.imageBox} src={image} alt='oops' /></div>
                <div className={styles.productData}>
                    <div className={styles.productTitle}>
                        {title}
                    </div>
                    <div className={styles.productDes}>
                        {description}
                    </div>
                    <div className={styles.productPrice}>
                        <span className={styles.price}>Price:</span>
                        <div className={styles.mydollar}>
                            <img className={styles.dollar} src="https://img.icons8.com/ios-glyphs/30/000000/us-dollar-circled--v1.png" /> {price}
                        </div>
                    </div>
                    <div className={styles.shop}>
                        <button className={styles.shopNow}>Shop Now</button>
                        <img src="https://img.icons8.com/ios-glyphs/30/000000/like--v2.png" />
                        <img src="https://img.icons8.com/ios-glyphs/30/000000/favorite-cart.png" />
                    </div>

                    <div className={styles.out}>

                        {this.state.isDeleted && <div>{availabe}</div>}
                    </div>
                </div>
            </div>
        )
    }
    componentDidMount() {

        fetch(`https://fakestoreapi.com/products/category/${this.props.type}`)
            .then(res => res.json())
            .then(
                (result) => {
                    // console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result,
                        isUpdated: false
                    });
                    // console.log(this.state.items)

                },
                (error) => {
                    this.setState({
                        isUpdated: false,
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, isUpdated, isDeleted, start } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className={styles.heart}><img src={heart} alt="loading.." /></div>;
        } else {
            return (
                <>
                    {isDeleted && <div className={styles.categories}>
                        {/* {this.state.isUpdated ? <div>{this.makeCategoryComp(this.state.updatedItem)}</div> : undefined} */}
                        {this.state.items.map(cat =>
                            //  Number(deleteProduct.id) === cat.id ? <div key={deleteProduct.id}>{this.makeCategoryComp(deleteProduct)}</div> : 
                            <div key={cat.id}>{this.makeCategoryComp(cat)}</div>)}
                    </div>}
                    {isUpdated && <div className={styles.categories}>
                        {/* {this.state.isUpdated ? <div>{this.makeCategoryComp(this.state.updatedItem)}</div> : undefined} */}
                        {this.state.items.map(cat =>
                            // Number(updatedItem.id) === cat.id ?
                            //     <div key={updatedItem.id}>{this.makeCategoryComp(updatedItem)}</div> :
                            <div key={cat.id}>{this.makeCategoryComp(cat)}</div>)}
                        {/* {this.state.items.map(cat => <div>{this.makeCategoryComp(cat)}</div>)} */}
                        <DeleteProduct func={this.deleteProduct} />
                    </div>}

                    {start && <div className={styles.categories}>
                        {this.state.items.map(cat => <div key={cat.id}>{this.makeCategoryComp(cat)}</div>)}
                        <UpdateProduct func={this.updateProduct} />
                    </div>}
                </>
            );
        }
    }
}